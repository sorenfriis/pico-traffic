import argparse
import os
import re
import serial
import sys
import time
from enum import Enum

default_log_file = os.getenv('APPDATA').replace('\\', '/') + '/Microsoft/Teams/logs.txt'

class Status(Enum):
    AVAILABLE = 0,
    BUSY = 1,
    DONOTDISTURB = 2,
    BERIGHTBACK = 3,
    AWAY = 4,
    ONTHEPHONE = 5,
    INAMEETING = 6,
    OFFLINE = 7,
    NEWACTIVITY = 8,
    PRESENTING = 9,
    OTHER = 10

StatusCommands = {
    Status.AVAILABLE    : 'g',
    Status.BUSY         : 'r',
    Status.DONOTDISTURB : 'r',
    Status.BERIGHTBACK  : 'y',
    Status.AWAY         : 'y',
    Status.ONTHEPHONE   : 'r',
    Status.INAMEETING   : 'r',
    Status.OFFLINE      : 'o',
    Status.NEWACTIVITY  : 'x',  # unused
    Status.PRESENTING   : 'r',
    Status.OTHER        : 'o'
}

IgnoredStatuses = [Status.NEWACTIVITY, Status.OTHER]

class ConnectionLostException(Exception):
    pass

class TeamsStatus:
    def __init__(self, log_file:str, serial_port:str):
        self.log_file = log_file
        self.serial = serial.Serial()
        self.serial.port = serial_port
        self.init_serial()
        self.set_light_from_status(Status.OFFLINE)
    
    def get_last_status_from_log_file(self):
        with open(self.log_file, 'r') as f:
            lines = f.readlines()
            for line in reversed(lines):
                if not self.is_status_line(line):
                    continue
                status = self.get_status(line)
                if status in IgnoredStatuses:
                    continue
                self.last_status = status
                return self.last_status
        return Status.OTHER

    @classmethod
    def is_status_line(cls, line:str):
        # Example line:
        # Mon Jun 06 2022 12:37:46 GMT+0200 (Central European Summer Time) <19748> -- info -- StatusIndicatorStateService: Added Busy (current state: Available -> Busy)| profileType: AAD
        return "StatusIndicatorStateService: Added" in line

    @classmethod
    def get_status(cls, status_line:str):
        pattern = '.*StatusIndicatorStateService: Added (\w+) .*'
        m = re.search(pattern, status_line)
        status_str = m.group(1).upper()
        try:
            status = Status[status_str]
        except:
            status = Status.OTHER
        return status

    def init_serial(self):
        self.serial.close()
        self.serial.open()

    def set_light_from_status(self, status:Status):
        if status in IgnoredStatuses:
            print("Ignored")
            return

        command = bytes(StatusCommands[status] + '\n', encoding='ascii')
        try:
            self.last_status = status
            self.serial.write(command)
        except serial.SerialException as e:
            print("Connection lost on", self.serial.portstr)
            raise ConnectionLostException()

    def set_light_from_log_file(self):
        lines = self.log_read_line() # Generator
        for line in lines:
            if not self.is_status_line(line):
                continue
            status = self.get_status(line)
            print(status)
            self.set_light_from_status(status)
    
    def log_read_line(self):
        with open(self.log_file, 'r') as f:
            f.seek(0, os.SEEK_END) # Go to the end of the file
            while True:
                line = f.readline()
                if not line:
                    time.sleep(0.1)
                    continue
                yield line
    
    def reconnect(self):
        print("Reconnecting ... ", end='')
        while True:
            try:
                self.init_serial()
                print("Connected!")
                return
            except:
                time.sleep(0.5)

def run(log_file:str, serial_port:str):
    ts = TeamsStatus(log_file, serial_port)
    last_status = ts.get_last_status_from_log_file()
    print("Last status : ", last_status)
    ts.set_light_from_status(last_status)
    while True:
        try:
            ts.set_light_from_log_file()
        except ConnectionLostException:
            ts.reconnect()
            ts.set_light_from_status(ts.last_status)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('serialport', help="E.g. COM4")
    parser.add_argument('--logfile', default = default_log_file)
    args = parser.parse_args()
    print("Log file    : ", args.logfile)
    print("Serial port : ", args.serialport)
    try:
        result = run(args.logfile, args.serialport)
        return result
    except Exception as e:
        print(e)
        return 1

if __name__ == "__main__":
    sys.exit(main())
