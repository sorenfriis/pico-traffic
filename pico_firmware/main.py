import ws2812b
import utime
import sys
import uselect
from machine import Pin

intensity = 0.5
pixels = ws2812b.ws2812b(num_leds=21, state_machine=0, pin=0)

def pin_change_handler(pin):
    # disable irq for debounce
    v = pin.value()
    pin.irq(trigger=0)
    utime.sleep(0.05)
    if pin.value() == v:
        next_scene()
    # re-enable irq
    pin.irq(pin_change_handler, trigger=Pin.IRQ_FALLING)

p1 = Pin(1, Pin.IN, Pin.PULL_UP)
p1.irq(pin_change_handler, trigger=Pin.IRQ_FALLING)

def limit(value, min, max):
    if value < min: value = min
    elif value > max: value = max
    return value


class color:
    def __init__(self, r,g,b):
        self.r = limit(r,0,255)
        self.g = limit(g,0,255)
        self.b = limit(b,0,255)
    
RED = color(255,0,0)
GREEN = color(0,255,0)
YELLOW = color(255,128,0)
OFF = color(0,0,0)

scenes = []

scene_off = [OFF]*21

scene_red = scene_off.copy()
for i in [4, 16]:
    scene_red[i] = RED

scene_yellow = scene_off.copy()
for i in [6, 18]:
    scene_yellow[i] = YELLOW

scene_green = scene_off.copy()
for i in [8, 20]:
    scene_green[i] = GREEN

scene_stop = scene_off.copy()
for i in [2, 13]:
    scene_stop[i] = RED

scene_walk = scene_off.copy()
for i in [0, 11]:
    scene_walk[i] = GREEN

scenes.append(scene_red)
scenes.append(scene_yellow)
scenes.append(scene_green)
scenes.append(scene_stop)
scenes.append(scene_walk)
scenes.append(scene_off)

scene_max = len(scenes)
scene_i = 0

def next_scene():
    global scene_i
    scene = scenes[scene_i]
    show_scene(scene)
    scene_i += 1
    if scene_i >= scene_max:
        scene_i = 0
 
def show_scene(scene):
    i = 0
    for color in scene:
        set_pixel(i, color, show=False)
        i += 1
    pixels.show()

def set_pixel(n, color, show=True):
    r = int(intensity * color.r)
    g = int(intensity * color.g)
    b = int(intensity * color.b)
    # print(n, r, g, b)
    pixels.set_pixel(n, r, g, b)
    if show:
        pixels.show()


for s in scenes:
    show_scene(s)
    utime.sleep(0.5)

input_buf = ""

def parse(buf):
    global intensity
    if len(buf) == 0: return

    print("input_buf:", buf)
    
    # Intensity
    if buf[0] == 'i':
       try:
           i = limit(float(buf[1:]), 0, 1)
           intensity = i
       except ValueError:
           pass
       print("Intensity: ", intensity)
    # Red
    elif buf[0] == 'r':
        show_scene(scene_red)
    # Yellow
    elif buf[0] == 'y':
        show_scene(scene_yellow)
    # Green
    elif buf[0] == 'g':
        show_scene(scene_green)
    # Stop
    elif buf[0] == 's':
        show_scene(scene_stop)
    # Walk
    elif buf[0] == 'w':
        show_scene(scene_walk)
    # Off
    elif buf[0] == 'o':
        show_scene(scene_off)
    # Set color for all pixels
    elif buf[0] == 'c':
        try:
            colors=buf[1:].split(',')
            if len(colors) != 3: return
            r = int(colors[0])
            g = int(colors[1])
            b = int(colors[2])
            pixels.fill(r,g,b)
            pixels.show()
        except Exception as e:
            print(e)
            pass

while True:
    # Read stdin
    list = uselect.select([sys.stdin], [], [], 0.01)
    if not list[0]:
        utime.sleep(0.01)
        continue
    byte = sys.stdin.read(1)
    
    if byte == '\n':
        parse(input_buf)
        input_buf = ""
        continue

    if len(input_buf) >= 20:
        input_buf = ""
    input_buf += byte


