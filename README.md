# Pico traffic

This project contains code to control a traffic light based on MS Teams status.\
The firmware for the traffic light is written in Micropython and written for a Raspberry Pi Pico.


## 1. Firmware for Raspberry Pi Pico  
The file `main.py` contains the Micropython firmware for Raspberry Pi.\
See [https://www.raspberrypi.com/documentation/microcontrollers/micropython.html](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html) for preparing the Raspberry Pi to run Micropython

### 1.1 Hardware
The Raspberry Pi Pico is configured with the following I/O's:
* GPIO 1 - Data output for WS2812B LED's
* GPIO 2 - Button Input (internal Pull-up activated)

### 1.2 Protocol
Communication with the device is done over the virtual serial port, that is available through the Raspberry Pi Pico's USB interface.\
On Windows this will typically be `COM1...COM4` \
On linux this will typically be `/dev/ttyACM0`

The device accepts the following commands:
```
r\n       Red:    Enable RED lights (in car direction)
y\n       Yellow: Enable YELLOW lights (in car direction)
g\n       Green:  Enable GREEN lights (in car direction)
w\n       Walk:   Enable GREEN lights (in walking direction)
s\n       Stop:   Enable RED lights (in walking direction)
o\n       Off:    Disable all lights

i[0-1]\n  Intensity: Set intensity level for all lights between 0 and 1 (float). 
          Example: i0.5\n
                   i1.0\n

c[0-255],[0-255],[0-255]\n
          Color:  Set color for ALL LED's. Meant for adjusting colors
          Example: c255,0,0\n
                   c128,255,128\n
```

## 2. Teams status
The file `teams_status.py` is the program that runs on PC to
- Detect the current status in MS Teams
- Send commands over Serial to the device

The program will by default read the status of MS Teams from
`%APPDATA%/Microsoft/Teams/logs.txt`

```
usage: teams_status.py [-h] [--logfile LOGFILE] serialport

positional arguments:
  serialport         E.g. COM4

optional arguments:
  -h, --help         show this help message and exit
  --logfile LOGFILE
```
